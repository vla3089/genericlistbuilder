package com.vlabs

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun View.inflate(@LayoutRes layoutRes: Int, parent: ViewGroup): View =
        LayoutInflater.from(this.context).inflate(layoutRes, parent, false)

fun <ViewType : View> RecyclerView.ViewHolder.bindView(id: Int) = lazy (LazyThreadSafetyMode.NONE){ itemView.findViewById<ViewType>(id) }