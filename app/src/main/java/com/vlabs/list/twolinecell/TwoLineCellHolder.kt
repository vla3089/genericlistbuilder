package com.vlabs.list.twolinecell

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.vlabs.genericlistbuilder.R
import com.vlabs.bindView
import com.vlabs.list.TwoLineCell

class TwoLineCellHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val title: TextView by bindView(R.id.title)
    val subtitle: TextView by bindView(R.id.subtitle)

    fun bind(cell: TwoLineCell) {
        title.text = cell.title
        subtitle.text = cell.subtitle
        itemView.setOnClickListener(cell.clickListener)
    }
}