package com.vlabs.list.twolinecell

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.vlabs.genericlistbuilder.R
import com.vlabs.inflate
import com.vlabs.list.AdapterDelegate
import com.vlabs.list.SimpleCell
import com.vlabs.list.TwoLineCell

class TwoLineCellAdapterDelegate<T> : AdapterDelegate<T> {
    override fun isForViewType(items: List<T>, position: Int) = items[position] is SimpleCell

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.two_line_cell, parent)
        return TwoLineCellHolder(view)
    }

    override fun bindViewHolder(holder: RecyclerView.ViewHolder, items: List<T>, position: Int) {
        val vh = holder as TwoLineCellHolder
        vh.bind(items[position] as TwoLineCell)
    }
}