package com.vlabs.list

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface AdapterDelegate<T> {
    fun isForViewType(items: List<T>, position: Int): Boolean

    fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun bindViewHolder(holder: RecyclerView.ViewHolder, items: List<T>, position: Int)
}