package com.vlabs.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.vlabs.list.dropdown.DropdownCellProxy

class ListActivity : AppCompatActivity() {

    private val root by lazy(LazyThreadSafetyMode.NONE) { RecyclerView(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        root.layoutManager = LinearLayoutManager(this)
        setContentView(root)

        val simpleCell = createSimpleCell("Some title")
        val dropdownCell = createDropdownCell()
        root.adapter = ListAdapter(listOf(simpleCell, dropdownCell))
    }

    private fun createDropdownCell() =
            DropdownCell("Dropdown cell title", createDropdownConfig())

    private fun createDropdownConfig(): DropdownConfig {
        val dropdownItems = arrayOf("Item 1", "Item 2123213123132", "Item 3")
        return DropdownConfig(createDropdownProxyFromArray(dropdownItems)) { position ->
            run {
                Toast.makeText(applicationContext, "${dropdownItems[position]} selected", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createDropdownProxyFromArray(dropdownItems: Array<String>): DropdownCellProxy {
        return object : DropdownCellProxy {
            override fun itemsCount(): Int = dropdownItems.size

            override fun getItem(position: Int): Any = dropdownItems[position]

            override fun getItemAsString(position: Int) = dropdownItems[position]

            override fun getItemId(position: Int): Long = position.toLong()

        }
    }

    private fun createSimpleCell(title: String) = SimpleCell(title,
            View.OnClickListener { Toast.makeText(applicationContext, title, Toast.LENGTH_LONG).show() })
}