package com.vlabs.list.dropdown

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class DropdownCellAdapter(
        context: Context,
        private val proxy: DropdownCellProxy,
        @LayoutRes private val selectedItemLayout: Int,
        @LayoutRes private val dropdownItemLayout: Int) : BaseAdapter() {

    private val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val itemView = convertView ?: inflater.inflate(selectedItemLayout, parent, false)
        val textView = itemView.findViewById<TextView>(android.R.id.text1)
        textView.text = proxy.getItemAsString(position)

        return itemView
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val itemView = convertView ?: inflater.inflate(dropdownItemLayout, parent, false)
        val textView = itemView.findViewById<TextView>(android.R.id.text1)
        textView.text = proxy.getItemAsString(position)

        return itemView
    }

    override fun getItem(position: Int): Any = proxy.getItem(position)

    override fun getItemId(position: Int): Long = proxy.getItemId(position)

    override fun getCount(): Int = proxy.itemsCount()
}