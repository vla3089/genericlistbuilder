package com.vlabs.list.dropdown

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.vlabs.genericlistbuilder.R
import com.vlabs.inflate
import com.vlabs.list.AdapterDelegate
import com.vlabs.list.DropdownCell

class DropdownCellAdapterDelegate<T> : AdapterDelegate<T> {
    override fun isForViewType(items: List<T>, position: Int) =
            items[position] is DropdownCell

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.dropdown_cell, parent)
        return DropdownCellHolder(view)
    }

    override fun bindViewHolder(holder: RecyclerView.ViewHolder, items: List<T>, position: Int) {
        val vh = holder as DropdownCellHolder
        vh.bind(items[position] as DropdownCell)
    }
}