package com.vlabs.list.dropdown

interface DropdownCellProxy {

    fun itemsCount(): Int

    fun getItem(position: Int): Any

    fun getItemAsString(position: Int): String

    fun getItemId(position: Int): Long
}