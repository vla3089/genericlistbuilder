package com.vlabs.list.dropdown

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView
import com.vlabs.bindView
import com.vlabs.genericlistbuilder.R
import com.vlabs.list.DropdownCell

class DropdownCellHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val title: TextView by bindView(R.id.title)
    val dropdown: Spinner by bindView(R.id.dropdown)

    fun bind(cell: DropdownCell) {
        title.text = cell.title

        dropdown.adapter = DropdownCellAdapter(
                itemView.context,
                cell.config.proxy,
                android.R.layout.simple_dropdown_item_1line,
                android.R.layout.simple_dropdown_item_1line)

        cell.config.initialSelection?.let {
            dropdown.setSelection(it)
        }

        dropdown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                cell.config.onItemSelected.invoke(position)
            }
        }
    }
}