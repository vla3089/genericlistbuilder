package com.vlabs.list.imagedcell

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.vlabs.genericlistbuilder.R
import com.vlabs.inflate
import com.vlabs.list.AdapterDelegate
import com.vlabs.list.CellWithImage

class CellWithImageAdapterDelegate<T> : AdapterDelegate<T> {
    override fun isForViewType(items: List<T>, position: Int) = items[position] is CellWithImage

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.cell_with_image, parent)
        return CellWithImageHolder(view)
    }

    override fun bindViewHolder(holder: RecyclerView.ViewHolder, items: List<T>, position: Int) {
        val vh = holder as CellWithImageHolder
        vh.bind(items[position] as CellWithImage)
    }
}