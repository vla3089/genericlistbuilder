package com.vlabs.list.imagedcell

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.vlabs.genericlistbuilder.R
import com.vlabs.bindView
import com.vlabs.list.CellWithImage

class CellWithImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val title: TextView by bindView(R.id.title)
    val image: ImageView by bindView(R.id.image)

    fun bind(cell: CellWithImage) {
        title.text = cell.title
        image.setImageResource(cell.resId)
        itemView.setOnClickListener(cell.clickListener)
    }
}