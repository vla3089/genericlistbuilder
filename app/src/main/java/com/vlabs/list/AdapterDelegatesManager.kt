package com.vlabs.list

import android.support.annotation.MainThread
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.vlabs.list.AdapterDelegate

@MainThread
class AdapterDelegatesManager<T> {

    private val delegates = LinkedHashMap<Int, AdapterDelegate<T>>()

    // TODO: support delegates removal. Remember about shifted indexes
    fun addDelegate(delegate: AdapterDelegate<T>) : AdapterDelegatesManager<T> {
        return also {
            if (notContains(delegate)) {
                val nextViewType = delegates.size
                delegates[nextViewType] = delegate
            }
        }
    }

    private fun notContains(delegate: AdapterDelegate<T>) =
            !delegates.containsValue(delegate)

    fun getItemViewType(items: List<T>, position: Int) =
            delegates.asSequence()
                    .filter { it.value.isForViewType(items, position) }
                    .firstOrNull()
                    ?.key ?: throw IllegalStateException("Adapter for position = $position is missing")

    fun createViewHolder(parent: ViewGroup, viewType: Int) =
            delegates[viewType]?.createViewHolder(parent)
                    ?: throw IllegalStateException("Adapter for $viewType is missing")

    fun bindViewHolder(holder: RecyclerView.ViewHolder, items: List<T>, position: Int) {
        delegates[holder.itemViewType]?.bindViewHolder(holder, items, position)
                ?: throw IllegalStateException("Adapter for ${holder.itemViewType}is missing")
    }
}

