package com.vlabs.list

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.vlabs.list.simplecell.SimpleCellAdapterDelegate
import com.vlabs.list.dropdown.DropdownCellAdapterDelegate
import com.vlabs.list.imagedcell.CellWithImageAdapterDelegate
import com.vlabs.list.twolinecell.TwoLineCellAdapterDelegate

class ListAdapter<T>(private val items: List<T>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val delegatesManager = AdapterDelegatesManager<T>()

    init {
        delegatesManager.addDelegate(SimpleCellAdapterDelegate())
            .addDelegate(DropdownCellAdapterDelegate())
            .addDelegate(CellWithImageAdapterDelegate())
            .addDelegate(TwoLineCellAdapterDelegate())
    }

    override fun getItemViewType(position: Int) =
        delegatesManager.getItemViewType(items, position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        delegatesManager.createViewHolder(parent, viewType)

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.bindViewHolder(holder, items, position)
    }
}