package com.vlabs.list.simplecell

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.vlabs.genericlistbuilder.R
import com.vlabs.inflate
import com.vlabs.list.AdapterDelegate
import com.vlabs.list.SimpleCell

class SimpleCellAdapterDelegate<T> : AdapterDelegate<T> {
    override fun isForViewType(items: List<T>, position: Int) = items[position] is SimpleCell

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.simple_cell, parent)
        return SimpleCellHolder(view)
    }

    override fun bindViewHolder(holder: RecyclerView.ViewHolder, items: List<T>, position: Int) {
        val vh = holder as SimpleCellHolder
        vh.bind(items[position] as SimpleCell)
    }
}