package com.vlabs.list.simplecell

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.vlabs.genericlistbuilder.R
import com.vlabs.bindView
import com.vlabs.list.SimpleCell

class SimpleCellHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val title: TextView by bindView(R.id.title)

    fun bind(cell: SimpleCell) {
        title.text = cell.title
        itemView.setOnClickListener(cell.clickListener)
    }
}