package com.vlabs.list

import android.support.annotation.DrawableRes
import android.view.Gravity
import android.view.View
import com.vlabs.list.dropdown.DropdownCellProxy

sealed class ListCell

class SimpleCell(val title: String, val clickListener: View.OnClickListener) : ListCell()

class TwoLineCell(val title: String,
                  val subtitle: String,
                  val clickListener: View.OnClickListener) : ListCell()

class CellWithImage(val title: String,
                    @DrawableRes val resId: Int,
                    val clickListener: View.OnClickListener) : ListCell()


class DropdownCell(val title: String,
                   val config: DropdownConfig) : ListCell()

class DropdownConfig(
    val proxy: DropdownCellProxy,
    val initialSelection: Int? = null,
    val gravity: Int = Gravity.START,
    inline val onItemSelected: (Int) -> Unit)